#!/usr/bin/python
# -*- coding: utf-8 -*-
##
# \file      csvGenerator.py
# This file defines the generation of csv-files for tikz
#
# \author    Florian Piewak <florian.piewak@daimler.com>
# \date      2017-01-05
# \copyright Daimler AG, RD/FAU, Team LiDAR

import numpy as np
import os
import glob
import collections

import sys

import csv

import operator
import math
import scipy



#------------------------------------------------------------------------------
# read a snapshot file and return the dictionary
#------------------------------------------------------------------------------
def readSnapshotEvaluation( filename ):
    data = np.load(filename)
    return data.item()


#------------------------------------------------------------------------------
# read all snapshot files and return the array
#------------------------------------------------------------------------------
def readAllSnapshotEvaluation( path ):
    data = []
    files = glob.glob(os.path.join(path, '*'))
    for file in files:
        if os.path.isfile(file):
            fileData = readSnapshotEvaluation(file)
            data.append(fileData)
    return data

#------------------------------------------------------------------------------
# create a csv file from a dict
#------------------------------------------------------------------------------
def createCSV( filename, dictValues ):
    #open file
    csvfile = open(filename, 'w')
    fieldnames = dictValues.keys()
    #convert names to suppress '_'
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    #write file
    writer.writeheader()
    #each row have to be an own dict
    for i in range(len(dictValues[dictValues.keys()[0]])):
        tempDict = {}
        for key in dictValues.keys():
            tempDict[key] = dictValues[key][i]
        writer.writerow(tempDict)
    csvfile.close()

#------------------------------------------------------------------------------
# convert dict a overall ordered dict
#------------------------------------------------------------------------------
def createDict( dictValuesArray ):
    additionalKeys = ['messing x MSE', 'messing y MSE', 'messing z MSE', 'messing yaw MSE', 'messing pitch MSE', 'messing roll MSE',
                      'messing translation MSE', 'messing rotation MSE',
                      'messing x MAE', 'messing y MAE', 'messing z MAE', 'messing yaw MAE', 'messing pitch MAE', 'messing roll MAE',
                      'messing translation MAE', 'messing rotation MAE']
    #create key mapping
    keyMapping = dict()
    #create dicr
    orderedDict = collections.OrderedDict([("iterationNumber", [])])
    #insert all other keys
    keys = dictValuesArray[0]['output'].keys()
    keys.extend(additionalKeys)
    for key in keys:
        convertedKey = key.replace('_', ' ')
        keyMapping[key] = convertedKey
        orderedDict.update([(convertedKey, [])])
    #fill dict
    for iterationDict in dictValuesArray:
        #insert iteration
        orderedDict['iterationNumber'].append(iterationDict['info']['iteration'])
        #insert data
        for key in iterationDict['output'].keys():
            orderedDict[keyMapping[key]].append(iterationDict['output'][key])

        #calculate the mse and mae for each value
        mseValues = [0, 0, 0, 0, 0, 0]
        maeValues = [0, 0, 0, 0, 0, 0]
        for customData in iterationDict['custom']:
            values = np.array(customData['messed'])
            valuesSquared = values * values
            valuesAbs = np.abs(values)
            mseValues = mseValues + valuesSquared
            maeValues = maeValues + valuesAbs
        #calculate mse and mae for rotation and translation
        mseTranslation = mseValues[0] + mseValues[1] + mseValues[2]
        mseRotation    = mseValues[3] + mseValues[4] + mseValues[5]
        maeTranslation = maeValues[0] + maeValues[1] + maeValues[2]
        maeRotation    = maeValues[3] + maeValues[4] + maeValues[5]
        #normalize
        mseValues      = np.sqrt(mseValues) / len(iterationDict['custom'])
        maeValues      = maeValues / len(iterationDict['custom'])
        mseTranslation = np.sqrt(mseTranslation) / ( 3 *len(iterationDict['custom']))
        mseRotation    = np.sqrt(mseRotation) / ( 3 *len(iterationDict['custom']))
        maeTranslation = maeTranslation / ( 3 *len(iterationDict['custom']))
        maeRotation    = maeRotation / ( 3 *len(iterationDict['custom']))

        #insert into dict
        orderedDict['messing x MSE'].append(mseValues[0])
        orderedDict['messing y MSE'].append(mseValues[1])
        orderedDict['messing z MSE'].append(mseValues[2])
        orderedDict['messing yaw MSE'].append(mseValues[3])
        orderedDict['messing pitch MSE'].append(mseValues[4])
        orderedDict['messing roll MSE'].append(mseValues[5])
        orderedDict['messing translation MSE'].append(mseTranslation)
        orderedDict['messing rotation MSE'].append(mseRotation)
        orderedDict['messing x MAE'].append(maeValues[0])
        orderedDict['messing y MAE'].append(maeValues[1])
        orderedDict['messing z MAE'].append(maeValues[2])
        orderedDict['messing yaw MAE'].append(maeValues[3])
        orderedDict['messing pitch MAE'].append(maeValues[4])
        orderedDict['messing roll MAE'].append(maeValues[5])
        orderedDict['messing translation MAE'].append(maeTranslation)
        orderedDict['messing rotation MAE'].append(maeRotation)

    return orderedDict

#------------------------------------------------------------------------------
# convert dict a overall ordered dict for only one snapshot
#------------------------------------------------------------------------------
def createDictSnapshot( dictValues , maxCount=-1):
    keys = ['gt x', 'gt y', 'gt z', 'gt yaw', 'gt pitch', 'gt roll',
            'net x', 'net y', 'net z', 'net yaw', 'net pitch', 'net roll',
            'diff x', 'diff y', 'diff z', 'diff yaw', 'diff pitch', 'diff roll',
            'diff abs x', 'diff abs y', 'diff abs z', 'diff abs yaw', 'diff abs pitch', 'diff abs roll',
            'mae translation', 'mae rotation', 'messing mae translation', 'messing mae rotation']
    #create dict
    orderedDict = collections.OrderedDict([("counter", []), ("file", [])])
    for key in keys:
        orderedDict.update([(key, [])])
    #read all the image iterations
    orderedDict = expandDictSnapshot(dictValues, orderedDict, maxCount)

    return orderedDict

#------------------------------------------------------------------------------
# convert dict a overall ordered dict for only one snapshot
#------------------------------------------------------------------------------
def expandDictSnapshot( dictValues , dictToExpand, maxCount=-1):
    #read all the image iterations
    counter = len(dictToExpand['counter'])
    for picture in dictValues['custom']:
        counter = counter + 1
        if maxCount > 0 and maxCount < counter:
            break
        dictToExpand['counter'].append(counter)
        dictToExpand['file'].append(picture['file'])
        dictToExpand['gt x'].append(picture['messed'][0])
        dictToExpand['gt y'].append(picture['messed'][1])
        dictToExpand['gt z'].append(picture['messed'][2])
        dictToExpand['gt yaw'].append(picture['messed'][3])
        dictToExpand['gt pitch'].append(picture['messed'][4])
        dictToExpand['gt roll'].append(picture['messed'][5])
        dictToExpand['net x'].append(picture['ours'][0])
        dictToExpand['net y'].append(picture['ours'][1])
        dictToExpand['net z'].append(picture['ours'][2])
        dictToExpand['net yaw'].append(picture['ours'][3])
        dictToExpand['net pitch'].append(picture['ours'][4])
        dictToExpand['net roll'].append(picture['ours'][5])
        dictToExpand['diff x'].append(picture['ours'][0] - picture['messed'][0])
        dictToExpand['diff y'].append(picture['ours'][1] - picture['messed'][1])
        dictToExpand['diff z'].append(picture['ours'][2] - picture['messed'][2])
        dictToExpand['diff yaw'].append(picture['ours'][3] - picture['messed'][3])
        dictToExpand['diff pitch'].append(picture['ours'][4] - picture['messed'][4])
        dictToExpand['diff roll'].append(picture['ours'][5] - picture['messed'][5])
        dictToExpand['diff abs x'].append(np.abs(picture['ours'][0] - picture['messed'][0]))
        dictToExpand['diff abs y'].append(np.abs(picture['ours'][1] - picture['messed'][1]))
        dictToExpand['diff abs z'].append(np.abs(picture['ours'][2] - picture['messed'][2]))
        dictToExpand['diff abs yaw'].append(np.abs(picture['ours'][3] - picture['messed'][3]))
        dictToExpand['diff abs pitch'].append(np.abs(picture['ours'][4] - picture['messed'][4]))
        dictToExpand['diff abs roll'].append(np.abs(picture['ours'][5] - picture['messed'][5]))
        #calculate mean for rotation and translation
        sumTrans = 0
        for i in (0, 1, 2):
            sumTrans = sumTrans + np.abs(picture['ours'][i] - picture['messed'][i])
        dictToExpand['mae translation'].append(sumTrans / 3.0)
        sumRot = 0
        for i in (3, 4, 5):
            sumRot = sumRot + np.abs(picture['ours'][i] - picture['messed'][i])
        dictToExpand['mae rotation'].append(sumRot / 3.0)
        sumTrans = 0
        for i in (0, 1, 2):
            sumTrans = sumTrans + np.abs(picture['messed'][i])
        dictToExpand['messing mae translation'].append(sumTrans / 3.0)
        sumRot = 0
        for i in (3, 4, 5):
            sumRot = sumRot + np.abs(picture['messed'][i])
        dictToExpand['messing mae rotation'].append(sumRot / 3.0)

    return dictToExpand

#------------------------------------------------------------------------------
# sort the dict to a certain index
#------------------------------------------------------------------------------
def sortDict( originDict, indexForSorting ):
    #sort the lists
    sortedArrays = [list(x) for x in zip(*sorted(zip(*[originDict[x] for x in originDict.keys()]), key=operator.itemgetter(indexForSorting)))]
    #reassign the arrays
    counter = 0
    for key in originDict.keys():
        originDict[key] = sortedArrays[counter]
        counter = counter + 1

    return originDict

#------------------------------------------------------------------------------
# downsample the values within a specific range
#------------------------------------------------------------------------------
def meanDownsample( originDict, downsamplefactor ):
    originLen = len(originDict[originDict.keys()[0]])
    for key in originDict.keys():
        if key != 'file':
            pad_size = math.ceil(float(len(originDict[key]))/downsamplefactor)*downsamplefactor - len(originDict[key])
            padded = np.append(originDict[key], np.zeros(pad_size) * np.NaN)
            originDict[key] = scipy.nanmean(padded.reshape(-1,downsamplefactor), axis=1)

    print 'downsampling from lenght {} to length {} successfull'.format(originLen, len(originDict[originDict.keys()[0]]))

    return originDict
        


#------------------------------------------------------------------------------
# main programm
#------------------------------------------------------------------------------
def main():
    # #check arguments
    # if len(sys.argv) == 1:
    #     print 'insert the path to the evaluation folder as an argument!'
    #     sys.exit()

    # #read the path
    # path = sys.argv[1]
    # if not os.path.isdir(path):
    #     print 'the inserted path ({}) is not a directory!'.format(path)
    #     sys.exit()

    # ################################################
    # # create data for input combinations - iteration plot
    # ################################################
    path = '/mapr/gpucluster.hks/user/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/xyzAngles_nohydra_1p5m_20deg_final/evaluation'
    data = readAllSnapshotEvaluation(path)
    orderedData = createDict(data)
    indexForSorting = orderedData.keys().index('iterationNumber')
    #indexForSorting = orderedData.keys().index('messing rotation MAE')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 50)
    createCSV('outputMesRotMAEangle.csv', orderedData)

    path = '/mapr/gpucluster.hks/user/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/quaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluation'
    data = readAllSnapshotEvaluation(path)
    orderedData = createDict(data)
    indexForSorting = orderedData.keys().index('iterationNumber')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 50)
    createCSV('outputMesRotMAEquaternion.csv', orderedData)

    path = '/mapr/gpucluster.hks/user/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluation'
    data = readAllSnapshotEvaluation(path)
    orderedData = createDict(data)
    indexForSorting = orderedData.keys().index('iterationNumber')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 50)
    createCSV('outputMesRotMAEdualquaternion.csv', orderedData)


    ################################################
    # create data for network iteration - mae rotation plot
    ################################################
    #iteration 3,000,000
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_50cm_5deg_init/evaluationAngles25/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = createDictSnapshot(data)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_50cm_5deg_init/evaluationAngles20/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_50cm_5deg_init/evaluationAngles15/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_50cm_5deg_init/evaluationAngles10/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_50cm_5deg_init/evaluationAngles5/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_50cm_5deg_init/evaluationAngles2/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_50cm_5deg_init/evaluationAngles1/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    indexForSorting = orderedData.keys().index('messing mae rotation')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 300)
    createCSV('snapshotEvalRotDualQuaternion5degIt3000000.csv', orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_20cm_2deg_init/evaluationAngles25/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = createDictSnapshot(data)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_20cm_2deg_init/evaluationAngles20/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_20cm_2deg_init/evaluationAngles15/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_20cm_2deg_init/evaluationAngles10/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_20cm_2deg_init/evaluationAngles5/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_20cm_2deg_init/evaluationAngles2/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_20cm_2deg_init/evaluationAngles1/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    indexForSorting = orderedData.keys().index('messing mae rotation')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 300)
    createCSV('snapshotEvalRotDualQuaternion2degIt3000000.csv', orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles25/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = createDictSnapshot(data)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles20/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles15/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles10/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles5/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles2/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles1/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    indexForSorting = orderedData.keys().index('messing mae rotation')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 300)
    createCSV('snapshotEvalRotDualQuaternion20degIt3000000.csv', orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1m_10deg_init/evaluationAngles25/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = createDictSnapshot(data)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1m_10deg_init/evaluationAngles20/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1m_10deg_init/evaluationAngles15/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1m_10deg_init/evaluationAngles10/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1m_10deg_init/evaluationAngles5/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1m_10deg_init/evaluationAngles2/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1m_10deg_init/evaluationAngles1/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    indexForSorting = orderedData.keys().index('messing mae rotation')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 300)
    createCSV('snapshotEvalRotDualQuaternion10degIt3000000.csv', orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_10cm_1deg_init/evaluationAngles25/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = createDictSnapshot(data)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_10cm_1deg_init/evaluationAngles20/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_10cm_1deg_init/evaluationAngles15/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_10cm_1deg_init/evaluationAngles10/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_10cm_1deg_init/evaluationAngles5/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_10cm_1deg_init/evaluationAngles2/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_10cm_1deg_init/evaluationAngles1/iteration_000003000000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    indexForSorting = orderedData.keys().index('messing mae rotation')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 300)
    createCSV('snapshotEvalRotDualQuaternion1degIt3000000.csv', orderedData)

    #best iteration for final eval
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_50cm_5deg_init/evaluationAngles25/iteration_000002888000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = createDictSnapshot(data)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_50cm_5deg_init/evaluationAngles20/iteration_000002888000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_50cm_5deg_init/evaluationAngles15/iteration_000002888000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_50cm_5deg_init/evaluationAngles10/iteration_000002888000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_50cm_5deg_init/evaluationAngles5/iteration_000002888000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_50cm_5deg_init/evaluationAngles2/iteration_000002888000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_50cm_5deg_init/evaluationAngles1/iteration_000002888000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    indexForSorting = orderedData.keys().index('messing mae rotation')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 300)
    createCSV('snapshotEvalRotDualQuaternion5degIt2888000.csv', orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_20cm_2deg_init/evaluationAngles25/iteration_000002946000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = createDictSnapshot(data)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_20cm_2deg_init/evaluationAngles20/iteration_000002946000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_20cm_2deg_init/evaluationAngles15/iteration_000002946000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_20cm_2deg_init/evaluationAngles10/iteration_000002946000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_20cm_2deg_init/evaluationAngles5/iteration_000002946000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_20cm_2deg_init/evaluationAngles2/iteration_000002946000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_20cm_2deg_init/evaluationAngles1/iteration_000002946000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    indexForSorting = orderedData.keys().index('messing mae rotation')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 300)
    createCSV('snapshotEvalRotDualQuaternion2degIt2946000.csv', orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles25/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = createDictSnapshot(data)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles20/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles15/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles10/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles5/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles2/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles1/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    indexForSorting = orderedData.keys().index('messing mae rotation')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 300)
    createCSV('snapshotEvalRotDualQuaternion20degIt2818000.csv', orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1m_10deg_init/evaluationAngles25/iteration_000002992000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = createDictSnapshot(data)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1m_10deg_init/evaluationAngles20/iteration_000002992000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1m_10deg_init/evaluationAngles15/iteration_000002992000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1m_10deg_init/evaluationAngles10/iteration_000002992000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1m_10deg_init/evaluationAngles5/iteration_000002992000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1m_10deg_init/evaluationAngles2/iteration_000002992000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1m_10deg_init/evaluationAngles1/iteration_000002992000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    indexForSorting = orderedData.keys().index('messing mae rotation')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 300)
    createCSV('snapshotEvalRotDualQuaternion10degIt2992000.csv', orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_10cm_1deg_init/evaluationAngles25/iteration_000002827000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = createDictSnapshot(data)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_10cm_1deg_init/evaluationAngles20/iteration_000002827000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_10cm_1deg_init/evaluationAngles15/iteration_000002827000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_10cm_1deg_init/evaluationAngles10/iteration_000002827000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_10cm_1deg_init/evaluationAngles5/iteration_000002827000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_10cm_1deg_init/evaluationAngles2/iteration_000002827000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_10cm_1deg_init/evaluationAngles1/iteration_000002827000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    indexForSorting = orderedData.keys().index('messing mae rotation')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 300)
    createCSV('snapshotEvalRotDualQuaternion1degIt2827000.csv', orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationMultipleAngles25/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = createDictSnapshot(data)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationMultipleAngles20/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationMultipleAngles15/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationMultipleAngles10/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationMultipleAngles5/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationMultipleAngles2/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationMultipleAngles1/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    indexForSorting = orderedData.keys().index('messing mae rotation')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 300)
    createCSV('snapshotEvalRotDualQuaternionRanges.csv', orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationMultipleRestrictAngles25/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = createDictSnapshot(data)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationMultipleRestrictAngles20/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationMultipleRestrictAngles15/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationMultipleRestrictAngles10/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationMultipleRestrictAngles5/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationMultipleRestrictAngles2/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationMultipleRestrictAngles1/iteration_000002818000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    indexForSorting = orderedData.keys().index('messing mae rotation')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 300)
    createCSV('snapshotEvalRotDualQuaternionRangesRestrict.csv', orderedData)


    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/quaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles25/iteration_000002400000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = createDictSnapshot(data)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/quaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles20/iteration_000002400000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/quaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles15/iteration_000002400000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/quaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles10/iteration_000002400000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/quaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles5/iteration_000002400000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/quaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles2/iteration_000002400000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/quaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationAngles1/iteration_000002400000.npy'
    data = readSnapshotEvaluation(path)
    orderedData = expandDictSnapshot(data, orderedData)
    indexForSorting = orderedData.keys().index('messing mae rotation')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 300)
    createCSV('snapshotEvalRotQuaternion.csv', orderedData)

    ################################################
    # create data for network iteration - mae rotation plot
    ################################################
    for i in range(10):
        num = i + 1
        # path = '/mapr/gpucluster.hks/user/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationFixed/iteration_000002818000_{:0>5}.npy'.format(num)
        path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationFixed/iteration_000002818000_{:0>5}.npy'.format(num)
        data = readSnapshotEvaluation(path)
        orderedData = createDictSnapshot(data, 2000)
        createCSV('snapshotFullDualQuaternionIt2818000_{:0>5}.csv'.format(num), orderedData)
        print 'finish calculating the full snapshot at fixed messing number {}'.format(num)


    ################################################
    # create calculation of general error over 100 iterations (offline)
    ################################################
    # path = '/mapr/gpucluster.hks/user/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationFixed'
    path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluationFixed'
    data = readAllSnapshotEvaluation(path)
    orderedData = createDict(data)
    indexForSorting = orderedData.keys().index('iterationNumber')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 100) #here should be statet the number of iterations per snapshot to calculate automotically the mean over different messings of one snapshot
    createCSV('outputTestTime.csv', orderedData)

    ################################################
    # create calculation of general error over 100 iterations (online)
    ################################################
    path = '/mapr/gpucluster.hks/user/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_20cm_2deg_init/evaluationFixed'
    data = readAllSnapshotEvaluation(path)
    orderedData = createDict(data)
    indexForSorting = orderedData.keys().index('iterationNumber')
    orderedData = sortDict(orderedData, indexForSorting)
    orderedData = meanDownsample(orderedData, 100) #here should be statet the number of iterations per snapshot to calculate automotically the mean over different messings of one snapshot
    createCSV('outputTestTimeOnline.csv', orderedData)

    
    #path = '/lhome/fpiewak/experiments_remote/licamRegNets/calibNet/singleExperiments/dualquaternionOut_normalized_inverseInitWithMean_1e5_1p5m_20deg_2/evaluation'
    #path = '/mapr/gpucluster.hks/user/schnick/experiments/licamRegNets/calibLearning/differentScales/xyzAngles_nohydra_50cm_5deg/evaluation'
    # path = '/mapr/gpucluster.hks/user/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_normalized_inverseInitWithMean_1e5/evaluation'
    # data = readAllSnapshotEvaluation(path)
    # orderedData = createDict(data)
    # #indexForSorting = orderedData.keys().index('iterationNumber')
    # indexForSorting = orderedData.keys().index('messing rotation MAE')
    # orderedData = sortDict(orderedData, indexForSorting)
    # orderedData = meanDownsample(orderedData, 50)
    # createCSV('outputMesRotMAE.csv', orderedData)

    # orderedData = createDict(data)
    # #indexForSorting = orderedData.keys().index('iterationNumber')
    # indexForSorting = orderedData.keys().index('messing rotation MSE')
    # orderedData = sortDict(orderedData, indexForSorting)
    # orderedData = meanDownsample(orderedData, 50)
    # createCSV('outputMesRotMSE.csv', orderedData)

    # orderedData = createDict(data)
    # indexForSorting = orderedData.keys().index('iterationNumber')
    # #indexForSorting = orderedData.keys().index('messing rotation MSE')
    # orderedData = sortDict(orderedData, indexForSorting)
    # orderedData = meanDownsample(orderedData, 50)
    # createCSV('outputIterationNumber.csv', orderedData)

    # orderedData = createDict(data)
    # #indexForSorting = orderedData.keys().index('iterationNumber')
    # indexForSorting = orderedData.keys().index('messing translation MAE')
    # orderedData = sortDict(orderedData, indexForSorting)
    # orderedData = meanDownsample(orderedData, 50)
    # createCSV('outputMesTransMAE.csv', orderedData)

    # orderedData = createDict(data)
    # #indexForSorting = orderedData.keys().index('iterationNumber')
    # indexForSorting = orderedData.keys().index('messing translation MSE')
    # orderedData = sortDict(orderedData, indexForSorting)
    # orderedData = meanDownsample(orderedData, 50)
    # createCSV('outputMesTransMSE.csv', orderedData)



    # path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluation/iteration_000001800000.npy'
    # data = readSnapshotEvaluation(path)
    # orderedData = createDictSnapshot(data)
    # createCSV('snapshotFix.csv', orderedData)

    # path = '/lhome/fpiewak/experiments/licamRegNets/calibNet/singleExperiments/dualquaternionOut_rotMult100_normalized_inverseInitWithMean_1e5_1p5m_20deg/evaluation/iteration_000001801000.npy'
    # data = readSnapshotEvaluation(path)
    # orderedData = createDictSnapshot(data)
    # indexForSorting = orderedData.keys().index('messing mae rotation')
    # orderedData = sortDict(orderedData, indexForSorting)
    # orderedData = meanDownsample(orderedData, 80)
    # createCSV('snapshotEvalRot.csv', orderedData)

    # orderedData = createDictSnapshot(data)
    # indexForSorting = orderedData.keys().index('messing mae translation')
    # orderedData = sortDict(orderedData, indexForSorting)
    # orderedData = meanDownsample(orderedData, 100)
    # createCSV('snapshotEvalTrans.csv', orderedData)



if __name__ == "__main__":
    main()