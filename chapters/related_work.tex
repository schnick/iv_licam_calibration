% -*- root: ../root.tex -*-
%
\label{sec:related_work}

Recently, many multi-sensor indoor~\cite{SONG15,SILBERMAN12} and
outdoor~\cite{MADDERN16,GEIGER13} datasets have been released, encouraging the
research community to advance the state-of-the-art in various scene
understanding tasks by exploiting multi-modal input data. Fusing sensor data
on a low-level requires a highly accurate registration of the various sensors.
Therefore, extrinsic calibration is an important field of research; especially
the registration of sensors with different modalities is challenging. In this
work, we focus on the calibration of camera and depth sensors due to their
relevance in the field of autonomous driving. Most state-of-the-art approaches
handle the 3D-2D registration between a camera and a depth sensor by using
special calibration targets~\cite{GEIGER12,MIRZAEI12,ZHANG04}. Other semi-automatic methods extract
human- selected 3D and 2D shapes from both sensors which are then
aligned~\cite{TAMAS13,TAMAS14}. The mentioned methods achieve excellent
results and can therefore be used for a suitable initial calibration. However,
they are either time consuming ~\cite{TAMAS13,TAMAS14,MIRZAEI12,ZHANG04}. or require a controlled environment~\cite{GEIGER12}.

Once a sensor system goes online and starts to operate, \eg as a product
or test fleet vehicle, external forces such as mechanical vibrations or
temperature changes may decrease the calibration quality. In this case, the
system has to detect and correct such decalibrations. This is referred to as
online calibration and has been investigated in several recent studies. In~\cite{BILESCHI09} for
example LiDAR scans are aligned to camera images by matching projected depth
edges to image edges. A similar approach is proposed by Levinson
\etal~\cite{LEVINSON13}. They calculate depth gradients on a LiDAR point cloud
and project those gradients onto an inverse distance transform of the edge
image. If strong gradients are associated to
pixels which are close to an image edge this results in a low energy. The
subsequent optimization determines the calibration parameters by energy
minimization. In a more recent work, Pandey \etal~\cite{PANDEY15} realize a
LiDAR-camera calibration by means of mutual information maximization. The
mutual information is computed by comparing the intensity readings of the
projected LiDAR points with the camera intensities. Chien \etal~\cite{CHIEN16}
identified weaknesses of the aforementioned approaches especially at highly
textured surfaces and shadows, which were wrongly used as targets.
Furthermore, the approaches could not deal with occlusions due to the sensor
displacements. Therefore, a visual-odometry driven online calibration is
proposed. They argue that the performance of the estimated ego-motion is
directly correlated to the quality of the extrinsic parameters. As the correct
ego-motion is unknown in their experiments, they evaluate the          inverse
re-projection error function. As the smoothness and convexity of this function
was not sufficient for a robust energy minimization they added constraints
using the approaches of Levinson \etal~\cite{LEVINSON13} and Pandey
\etal~\cite{PANDEY15}. The combination leads to stable results if the
calibration is disturbed by not more than \SI{2}{\degree} and
\SI{10}{\centi\metre}. However, we experienced that the energy minimization
often gets stuck in local minima which is why the approach cannot compensate
for larger errors. Furthermore, the approach is not real-time capable as it
solves for the visual odometry and extrinsic parameters iteratively using the
Levenberg-Marquardt algorithm and gradient-descent.

At the same time, deep learning has been successfully applied to classic
computer vision tasks such as optical flow~\cite{DOSOVITSKIY16,ILG16} or
stereo estimation~\cite{ZBONTAR16,MAYER16}. Kendall \etal~\cite{KENDALL15}
train a network to regress a \num{6} degree of freedom (DOF) camera pose.
Ummenhofer \etal~\cite{UMMENHOFER16} combine elements of the aforementioned
and estimate depth, flow, and ego-motion to calculate structure-from-motion
using an end-to-end trained deep neural network. Surprisingly, there are only
few works leveraging the strength of deep learning for calibration. Workman
\etal~\cite{WORKMAN15} estimate the focal length of their system given natural
images whereas Giering \etal~\cite{GIERING15} use multi-modal CNNs for real-
time LiDAR-video registration. By concatenating flow, RGB and LiDAR depth
patches they solve a \num{9}-class classification problem where each class
corresponds to a particular $x$-$y$ shift on an ellipse. However, to the best
of our knowledge, there exists no deep learning-based approach that directly
regresses the calibration parameters.

In this work, we leverage the strength of deep neural networks for feature
extraction, feature matching, and regression to estimate the extrinsic
calibration parameters of a multi-modal sensor system. To this end, we propose
\emph{RegNet} based on our main contributions:
\begin{enumerate}
\item A CNN that directly regresses extrinsic calibration parameters for all \num{6} DOF;
\item an effective training strategy, which needs only one manually calibrated sensor setup;
\item a real-time, low-memory network, which can be easily deployed in autonomous vehicles.
\end{enumerate}