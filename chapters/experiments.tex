% -*- root: ../root.tex -*-
%
\label{sec:experiments}

\begin{figure}[t]
	\centering%
	\iterationPlot{tools/outputMesRotMAEdualquaternion.csv}[tools/outputMesRotMAEangle.csv][tools/outputMesRotMAEquaternion.csv]%
	\caption{Development of the mean absolute error (MAE) of the rotational components over training iteration for different output representations: Euler angles are represented in red, quaternions in brown and dual quaternions in blue. Both quaternion representations outperform the Euler angles representation.}
	\label{fig:iterationRepresenationEval}
\end{figure}

To evaluate our approach, we perform several experiments on real sensor data. As we are most interested in sensors that are relevant for autonomous driving, we focus on the calibration of a LiDAR-camera setup in this section. During our experiments we noticed that the rotational components have a larger impact on the quality of the resulting registration and are also harder to determine by our network. Therefore, our comparisons between different methods are mainly based on the rotational components.

\begin{figure}[t]
	\centering%
	\messingPlotMAErotSnapsot{tools/snapshotEvalRotDualQuaternion20degIt2818000.csv}[tools/snapshotEvalRotDualQuaternion10degIt2992000.csv][tools/snapshotEvalRotDualQuaternion5degIt2888000.csv][tools/snapshotEvalRotDualQuaternion2degIt2946000.csv][tools/snapshotEvalRotDualQuaternion1degIt2827000.csv][tools/snapshotEvalRotDualQuaternionRanges.csv]%
	\caption{Analysis of the calibration performance (rotation only) over the decalibration magnitude for different networks.
	The networks have been trained on random initial decalibrations, varying from 0.1m / 1$\SI{}{\degree}$  to 1.5m / 20$\SI{}{\degree}$.
	It can be seen that the networks perform better on certain decalibrations, depending on the range they have been trained on. 
	Therefore, an iterative execution of experts is proposed.}
	\label{fig:differentMessingScales}
\end{figure}

\subsection{Dataset}
We evaluate our approach on the KITTI dataset \cite{GEIGER13}, which provides $1392\times512$ pixel RGB images as well as depth measurements from a Velodyne HDL-64E LiDAR scanner. The extrinsic parameters of the dataset were calculated using the method of \cite{GEIGER12} and serve as ground truth for our experiments. For training, validation and testing we use the raw sequences of KITTI dataset where for each recording day different intrinsic and extrinsic calibrations were calculated. To reduce inconsistencies caused by calibration noise, we only use sequences of the first recording day (09/26/2011) for training and validation. For validation we select two challenging sequences (drive 0005 and 0070 with 574 frames in total) while all other sequences are only used for training (14863 frames). We randomly vary $\decalib$ for each frame during training as described in \Cref{sec:method:data_representation}, yielding a potentially infinite amount of training data. The final testing (\Cref{sec:experiments:temporalFiltering}) is performed on a separate day and sequence (09/30/2011 drive 0028 with 5177 frames) to create an independent test set. We chose this sequence as it contains a huge variety of different scenes.

\subsection{Data Representation}

\begin{figure*}[t]
  \centering
  \begin{subfigure}[b]{0.32\textwidth}
  	\caption*{Initial Calibration}
    \includegraphics[width=\textwidth]{figures/png/outfile_000000000147_final_messed.png}
    \label{fig:iterativeRefinement_messed}
  \end{subfigure}
  %
  \begin{subfigure}[b]{0.32\textwidth}
	\caption*{After $\SI{20}{\degree}$/$\SI{1.5}{\metre}$ network}
    \includegraphics[width=\textwidth]{figures/png/outfile_000000000147_additional000_ours.png}
    \label{fig:iterativeRefinement1}
  \end{subfigure}
  %
  \begin{subfigure}[b]{0.32\textwidth}
    \caption*{After $\SI{10}{\degree}$/$\SI{1.0}{\metre}$ network}
    \includegraphics[width=\textwidth]{figures/png/outfile_000000000147_additional001_ours.png}
    \label{fig:iterativeRefinement2}
  \end{subfigure}
  %
  \begin{subfigure}[b]{0.32\textwidth}
    \caption*{After $\SI{5}{\degree}$/$\SI{0.5}{\metre}$ network}
    \includegraphics[width=\textwidth]{figures/png/outfile_000000000147_additional002_ours.png}
    \label{fig:iterativeRefinement3}
  \end{subfigure}
  %
  \begin{subfigure}[b]{0.32\textwidth}
    \caption*{After $\SI{2}{\degree}$/$\SI{0.2}{\metre}$ network}
    \includegraphics[width=\textwidth]{figures/png/outfile_000000000147_additional003_ours.png}
    \label{fig:iterativeRefinement4}
  \end{subfigure}
  %
  \begin{subfigure}[b]{0.32\textwidth}
    \caption*{After $\SI{1}{\degree}$/$\SI{0.1}{\metre}$ network}
    \includegraphics[width=\textwidth]{figures/png/outfile_000000000147_final_ours.png}
    \label{fig:iterativeRefinement_final}
  \end{subfigure}
  %
  % \begin{subfigure}[b]{0.32\textwidth}
  %   \includegraphics[width=\textwidth]{figures/png/outfile_000000000147_final_gt.png}
  %   \caption{Ground truth}
  %   \label{fig:iterativeRefinement_gt}
  % \end{subfigure}
  \caption{For the iterative refinement, the estimated calibration of one expert network is used to improve the projection of the depth points. The refined depth map is then forwarded to the next network. From top left to bottom right we can see a constant improvement in each iteration step.}
  \label{fig:iterativeRefinement}
\end{figure*}


\begin{figure*}[t]
	\centering%
	
	\begin{subfigure}{0.49\textwidth}
        \centering
		\caption*{Initial Calibration}
		\includegraphics[width=0.8\textwidth]{figures/png/outfile_000000000191_timeOut_messed.png}
		\caption*{Calibration result}
		\includegraphics[width=0.8\textwidth]{figures/png/outfile_000000000191_timeOut_ours.png}
		%\caption*{Distribution of calibration error}
		\insertBoxplot{tools/snapshotFullDualQuaternionIt2818000_00001.csv}%
	\end{subfigure} %
	\begin{subfigure}{0.49\textwidth}
        \centering
		\caption*{Initial Calibration}
		\includegraphics[width=0.8\textwidth]{figures/png/outfile_000000000258_timeOut_messed.png}
		\caption*{Calibration result}
		\includegraphics[width=0.8\textwidth]{figures/png/outfile_000000000258_timeOut_ours.png}
		%\caption*{Distribution of calibration error}
		\insertBoxplot{tools/snapshotFullDualQuaternionIt2818000_00002.csv}%
	\end{subfigure} %
	\caption{Examples of the Distribution of the calibration error for a decalibration, which is fixed over the test sequence. Five networks are executed iteratively ($\SI{20}{\degree}$/$\SI{1.5}{\metre}$, $\SI{10}{\degree}$/$\SI{1.0}{\metre}$, $\SI{5}{\degree}$/$\SI{0.5}{\metre}$, $\SI{2}{\degree}$/$\SI{0.2}{\metre}$ and $\SI{1}{\degree}$/$\SI{0.1}{\metre}$).}
	\label{fig:boxplotTimeFiltering}
\end{figure*}

\begin{figure*}[t]
	\centering%
	\centering%
	\begin{subfigure}[b]{0.32\textwidth}
	\caption*{Initial Calibration}
		\includegraphics[width=\textwidth]{figures/png/outfile_000000003004_final_messed.png}
	\end{subfigure}
	%
	\begin{subfigure}[b]{0.32\textwidth}
		\caption*{Ground Truth}
		\includegraphics[width=\textwidth]{figures/png/outfile_000000003004_final_gt.png}
	\end{subfigure}
	%
	\begin{subfigure}[b]{0.32\textwidth}
		\caption*{\emph{RegNet} Calibration}
		\includegraphics[width=\textwidth]{figures/png/outfile_000000003004_final_ours.png}
	\end{subfigure}

	\vspace{1mm}

	\begin{subfigure}[b]{0.32\textwidth}
		\includegraphics[width=\textwidth]{figures/png/outfile_000000003113_final_messed.png}
	\end{subfigure}
	%
	\begin{subfigure}[b]{0.32\textwidth}
		\includegraphics[width=\textwidth]{figures/png/outfile_000000003113_final_gt.png}
	\end{subfigure}
	%
	\begin{subfigure}[b]{0.32\textwidth}
		\includegraphics[width=\textwidth]{figures/png/outfile_000000003113_final_ours.png}
	\end{subfigure}
	\caption{Examples of calibration results for an online scenario based on decalibrations up to $0.2 m$ and $2 ^\circ$, where only two networks are executed iteratively ($\SI{2}{\degree}$/$\SI{0.2}{\metre}$ and $\SI{1}{\degree}$/$\SI{0.1}{\metre}$ network)}
	\label{fig:onlineScenario}
\end{figure*}

The representation of the decalibration $\decalib$ is critical for the performance of our method. In this section, we compare the results of three different representations: Euler angles with translation, quaternions with translation and dual quaternions. Each representation is normalized as described in \Cref{sec:method:data_representation}. We analyzed the distribution of the real part $\realPartDQ$ of the dual quaternion within our decalibration range of $20^\circ$ and determined the factor $f=100$ to balance the dual quaternions. We also found that this factor gained the best results using quaternions with translation. Larger values of $f$ result in volatile translation whereas smaller values result in worse rotation estimates.

\Cref{fig:iterationRepresenationEval} shows the mean absolute error of the estimated rotation. Both quaternion representations outperform the Euler angles. However, the curve progression of both quaternion representations suggests that dual quaternions will have a higher performance at longer training time. Subsequent experiments are therefore performed with dual quaternions only.

\subsection{Different Decalibration Ranges}
During training we challenge the network to compensate for random decalibrations in the range of $[\SI{-1.5}{\metre},\SI{1.5}{\metre}]$ and $[\SI{-20}{\degree},\SI{20}{\degree}]$. The Euclidean loss penalizes strong deviations which is why large decalibrations have a bigger impact on the network than small ones. This results in a worse relative improvement for small decalibrations which is depicted in \Cref{fig:differentMessingScales}. To compensate this effect, we train expert networks on different decalibration ranges. These ranges are based on the worst mean absolute error (MAE) of the network, which is trained on the next larger range, to reach high robustness. We determine the following ranges: $\left[ -x, x \right]$ / $\left[ -y, y \right]$ (translation / rotation) for $x = \{\SI{1.5}{\metre}, \SI{1.0}{\metre}, \SI{0.5}{\metre}, \SI{0.2}{\metre}, \SI{0.1}{\metre} \}$ and $y = \{\SI{20}{\degree}, \SI{10}{\degree}, \SI{5}{\degree}, \SI{2}{\degree}, \SI{1}{\degree} \}$.

\Cref{fig:differentMessingScales} shows how these expert networks perform on varying decalibration magnitudes. It can be seen that choosing the best network is dependent on the decalibration. However, as we do not know the decalibration outside of our test environment, we perform an iterative refinement as described in \Cref{sec:method:refinement} starting with the $\SI{20}{\degree}/\SI{1.5}{\metre}$ network followed by the $\SI{10}{\degree}/\SI{1.0}{\metre}$, $\SI{5}{\degree}/\SI{0.5}{\metre}$, $\SI{2}{\degree}/\SI{0.2}{\metre}$ and $\SI{1}{\degree}/\SI{0.1}{\metre}$ network, respectively. A result of this iterative refinement is shown in \Cref{fig:iterativeRefinement}.
The execution time of the iterative approach is real-time capable with $\SI{7.3}{\milli\second}$ for one network forward pass on an NVIDIA TITAN X (Pascal architecture).

The order of the networks is optimized for decalibration scenarios up to $\SI{1.5}{\metre}$ and $\SI{20}{\degree}$ which can be used for calibrating a sensor from scratch. In online calibration scenarios however, the decalibrations are much smaller. In this case the number of networks for iterative execution can be decreased. \Cref{fig:onlineScenario} shows an online scenario of random decalibrations up to $\SI{20}{\centi\metre}$ and $\SI{2}{\degree}$, where only two networks with decalibration ranges $\left[ -x, x \right]$ / $\left[ -y, y \right]$ (translation / rotation) for $x = \{\SI{0.2}{\metre}, \SI{0.1}{\metre} \}$ and $y = \{\SI{2}{\degree}, \SI{1}{\degree} \}$ are executed. Within this online scenario we reach the same performance as in the offline scenario while decreasing the execution time by using only two networks iteratively.

\begin{figure*}[t]
	\centering%
	\begin{subfigure}[c]{0.329\textwidth}
		\caption*{Initial Calibration}
        \begin{tikzpicture}
        \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=\textwidth]{figures/png/outfile_000000000506_final_messed.png}};
        \draw[red,ultra thick,rounded corners] (1.75,1.3) rectangle (4.2,0.2);
        \end{tikzpicture}
		%\includegraphics[width=\textwidth]{figures/png/outfile_000000000506_final_messed.png}
	\end{subfigure}
	%
	\begin{subfigure}[c]{0.329\textwidth}
		\caption*{Ground Truth (cropped)}
		\includegraphics[trim=20cm 3cm 20cm 9cm, clip, width=\textwidth]{figures/png/outfile_000000000506_final_gt.png}
	\end{subfigure}
	%
	\begin{subfigure}[c]{0.329\textwidth}
		\caption*{\emph{RegNet} Calibration (cropped)}
		\includegraphics[trim=20cm 3cm 20cm 9cm, clip, width=\textwidth]{figures/png/outfile_000000000506_final_ours.png}
	\end{subfigure}

	\vspace{1mm}

	\begin{subfigure}[c]{0.329\textwidth}
        \begin{tikzpicture}
        \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=\textwidth]{figures/png/outfile_000000000591_final_messed.png}};
        \draw[red,ultra thick,rounded corners] (2.5,1.3) rectangle (4.2,0.5);
        \end{tikzpicture}
		%\includegraphics[width=\textwidth]{figures/png/outfile_000000000591_final_messed.png}
	\end{subfigure}
	%
	\begin{subfigure}[c]{0.329\textwidth}
		\includegraphics[trim=35cm 8cm 20cm 9cm, clip, width=\textwidth]{figures/png/outfile_000000000591_final_gt.png}
	\end{subfigure}
	%
	\begin{subfigure}[c]{0.329\textwidth}
		\includegraphics[trim=35cm 8cm 20cm 9cm, clip, width=\textwidth]{figures/png/outfile_000000000591_final_ours.png}
	\end{subfigure}
	
	\vspace{1mm}
	
	\begin{subfigure}[c]{0.329\textwidth}

        \begin{tikzpicture}
        \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=\textwidth]{figures/png/outfile_000000000755_final_messed.png}};
        \draw[red,ultra thick,rounded corners] (1.35,1.3) rectangle (3.8,0.2);
        \end{tikzpicture}
		%\includegraphics[width=\textwidth]{figures/png/outfile_000000000755_final_messed.png}
	\end{subfigure}
	%
	\begin{subfigure}[c]{0.329\textwidth}
		\includegraphics[trim=15cm 3cm 25cm 9cm, clip, width=\textwidth]{figures/png/outfile_000000000755_final_gt.png}
	\end{subfigure}
	%
	\begin{subfigure}[c]{0.329\textwidth}
		\includegraphics[trim=15cm 3cm 25cm 9cm, clip, width=\textwidth]{figures/png/outfile_000000000755_final_ours.png}
	\end{subfigure}
	
	\vspace{1mm}
	
	\begin{subfigure}[c]{0.329\textwidth}
        \begin{tikzpicture}
            \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=\textwidth]{figures/png/outfile_000000001574_final_messed.png}};
            \draw[red,ultra thick,rounded corners] (2.15,1.3) rectangle (3.8,0.6);
        \end{tikzpicture}
		%\includegraphics[width=\textwidth]{figures/png/outfile_000000001574_final_messed.png}
	\end{subfigure}
	%
	\begin{subfigure}[c]{0.329\textwidth}
		\includegraphics[trim=30cm 9cm 30cm 9cm, clip, width=\textwidth]{figures/png/outfile_000000001574_final_gt.png}
	\end{subfigure}
	%
	\begin{subfigure}[c]{0.329\textwidth}
		\includegraphics[trim=30cm 9cm 30cm 9cm, clip, width=\textwidth]{figures/png/outfile_000000001574_final_ours.png}
	\end{subfigure}
	
	\vspace{1mm}
	
	\begin{subfigure}[c]{0.329\textwidth}
        \begin{tikzpicture}
            \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=\textwidth]{figures/png/outfile_000000001594_final_messed.png}};
            \draw[red,ultra thick,rounded corners] (1.75,1.3) rectangle (4.2,0.2);
        \end{tikzpicture}
		%\includegraphics[width=\textwidth]{figures/png/outfile_000000001594_final_messed.png}
	\end{subfigure}
	%
	\begin{subfigure}[c]{0.329\textwidth}
		\includegraphics[trim=20cm 3cm 20cm 9cm, clip, width=\textwidth]{figures/png/outfile_000000001594_final_gt.png}
	\end{subfigure}
	%
	\begin{subfigure}[c]{0.329\textwidth}
		\includegraphics[trim=20cm 3cm 20cm 9cm, clip, width=\textwidth]{figures/png/outfile_000000001594_final_ours.png}
	\end{subfigure}
	
	\caption{Results of different single shot calibration results on the test set. Five networks, trained on different decalibration ranges ($\SI{20}{\degree}$/$\SI{1.5}{\metre}$, $\SI{10}{\degree}$/$\SI{1.0}{\metre}$, $\SI{5}{\degree}$/$\SI{0.5}{\metre}$, $\SI{2}{\degree}$/$\SI{0.2}{\metre}$ and $\SI{1}{\degree}$/$\SI{0.1}{\metre}$), are executed iteratively. Although the initial calibration is extremely bad, the proposed method delivers accurate results.}
	\label{fig:extremeScenarios}
\end{figure*}

\subsection{Temporal Filtering}
\label{sec:experiments:temporalFiltering}

The previous experiments are based on only one frame and can be noisy due to missing structure and sensor artifacts like rolling shutter or dynamic objects.
This can be further improved by analyzing the results over time as mentioned in \Cref{sec:method:refinement}. For this purposes, we determine the distribution of our results over the whole test sequence, while keeping the decalibration fixed. \Cref{fig:boxplotTimeFiltering} visualizes two examples of the distributions of the individual components by means of boxplots. In general, the estimated decalibrations $\decalibguess$ are distributed well around the ground truth values. Taking the median over the whole sequence resulted in the best performance on the validation set. For the quantitative evaluation on the test set we sampled decalibrations in the range of $\left[ \SI{-20}{\degree}, \SI{20}{\degree} \right]$ / $\left[ \SI{-1.5}{\metre}, \SI{1.5}{\metre} \right]$. The decalibration is kept fixed for one pass of the test set and then resampled. In total we performed 100 runs on the test set with different decalibrations. Our approach achieves a mean angle error of $\SI{0.28}{\degree}$ (yaw, pitch, roll: $\SI{0.24}{\degree}$, $\SI{0.25}{\degree}$, $\SI{0.36}{\degree}$) and a mean translation error of $\SI{6}{\centi\meter}$ (x, y, z: $\SI{7}{\centi\meter}$, $\SI{7}{\centi\meter}$, $\SI{4}{\centi\meter}$). In \Cref{fig:titlePicture} and \Cref{fig:extremeScenarios} results of our approach are visualized. It can be seen that the network is capable of handling even large decalibrations from the ground truth.