% -*- root: ../root.tex -*-
%
\label{sec:method}
\begin{figure*}[t]
    \centering
    \scalebox{0.73}{%
    \input{figures/tikz/network_layout.tex}
    }
    \caption{Our method estimates the calibration between a
    depth and an RGB sensor. The depth points are projected on
    the RGB image using an initial calibration $\mat{H}_{\text{init}}$.
    In the first and second part of the network we use NiN blocks to extract
    rich features for matching. The kernel size $k$ of the first convolutional layer of the NiN block is displayed by the indices. The number of feature channels is shown in the top right corner of each layer module. The final part regresses the decalibration by gathering
    global information using two fully connected layers. During
    training $\decalib$ is randomly permutated resulting in
    different projections of the depth points.}
    \label{fig:networkstructure}
\end{figure*}
The goal of this work is to develop a generic approach for
extrinsic sensor calibration. For this purpose we leverage deep neural networks for feature extraction and matching like proposed by Dosovitskiy \etal~\cite{DOSOVITSKIY16} and Ilg \etal~\cite{ILG16} and regress a full 6 DOF extrinsic calibration which is motivated by the work of Kendall \etal~\cite{KENDALL15}.

Although our approach can generally be applied to different
sensor modalities and combinations, we focus on
LiDAR-camera calibration in this work due to their important
role in autonomous driving. In the following sections we discuss
the data representation of the network inputs, the design of
the CNN, subsequent refinement steps, and training details.
\begin{figure*}[t]
  \centering
  \begin{subfigure}[b]{0.329\textwidth}
    \includegraphics[width=\textwidth]{figures/png/outfile_000000000002_messed.png}
    \label{fig:differentMessings1}
  \end{subfigure}
  %
  \begin{subfigure}[b]{0.329\textwidth}
    \includegraphics[width=\textwidth]{figures/png/outfile_000000000007_messed.png}
    \label{fig:differentMessings2}
  \end{subfigure}
  %
  \begin{subfigure}[b]{0.329\textwidth}
    \includegraphics[width=\textwidth]{figures/png/outfile_000000000048_messed.png}
    \label{fig:differentMessings3}
  \end{subfigure}
  \caption{We deviate the initial calibration up to \SI{20}{\degree} in rotation and up to \SI{1.5}{\metre} in translation from the ground truth calibration. This might result in projections of the LiDAR points where most of the points are outside the image area and it is therefore difficult to establish correspondences with the RGB image.}
  \label{fig:differentMessings}
\end{figure*}

\subsection{Data Collection and Representation}
\label{sec:method:data_representation}
The performance of deep learning methods improve with the accuracy and amount of data presented to them. In our case we would need pairs of images and LiDAR scans accompanied with a ground truth calibration.
However, determining the ground truth for thousands of differently arranged LiDAR-camera pairs would be bothersome.
We therefore reformulate the problem of extrinsic calibration as determining the decalibration $\decalib$ given an
initial calibration $\mat{H}_{\text{init}}$ and a
ground truth calibration $\mat{H}_{\text{gt}}$. We can then vary $\mat{H}_{\text{init}}$ randomly to get a huge amount of training data.

To be able to establish correspondences, the LiDAR points
are projected on the camera frame using $\mat{H}_{\text{init}}$ and
the intrinsic camera matrix $\mat{P}$, \ie
\begin{equation} z_c
\begin{bmatrix}
u \\ v \\ 1
\end{bmatrix} = \mat{P}\ \mat{H}_{\text{init}}\ \vec{x}\ .
\end{equation}
At each pixel $(u,v)$ we store the inverse depth of the projected point (in camera coordinates) $z_c$ or zero if no LiDAR point was projected on that particular pixel.
As most common LiDAR sensors provide only few measurements in comparison to the amount of image pixels, the depth images are
quite sparse. To account for the sparsity, we upsample the
projected LiDAR points by using max pooling on the input depth
map. The LiDAR depth image as well as the camera image are mean adjusted.

The ground truth decalibration can be represented in various ways.
The homogeneous decalibration matrix $\decalib$ is composed
of a $3\times3$ rotation matrix $\mat{R}$ and a $3\times1$ translation vector $\vec{t}$:

\begin{equation}
\decalib = \begin{bmatrix}
& \mat{R} & & \vec{t} \\
0 & 0 & 0 & 1
\end{bmatrix}
\end{equation}

To reduce the amount of learned parameters, the rotation can
be represented by Euler angles. Another option would be to
use quaternions like proposed in \cite{KENDALL15}. However,
Euler angles and quaternions share the disadvantage of
decoupled rotation and translation parameters. With dual-quaternions
a unified representation of translation and rotation can be
achieved. A dual-quaternion $\vec{\sigma}$ is composed of a real part $\realPartDQ$ and a dual part $\dualPartDQ$:

\begin{equation}
\vec{\sigma} = \realPartDQ + \epsilon \dualPartDQ
\end{equation}

where $\realPartDQ$ contains rotational and $\dualPartDQ$ rotational and translational information.
The decalibration values are normalized to the range $[-1, 1]$. For quaternions we can use the normalized form with $\left\| \realPartDQ \right\| = 1$.
For dual quaternions $\dualPartDQ$ is represented with values without a specific range.
This results in an imbalance of the dual quaternions during training.
To compensate this effect, we multiply the values of $\realPartDQ$ by a factor $f$.
This also creates an implicit weighting of the rotational part for the loss function of the CNN.

\subsection{Network Architecture}
\label{sec:method:network_architecture}

We design our network to solve the tasks of feature extraction, feature matching and regression of the calibration parameters. All three steps are combined in only one CNN which can be trained end-to-end. The block diagram in \Cref{fig:networkstructure} shows the outline of \emph{RegNet} and it's embedding in the training pipeline.

Due to their fast convergence we constructed the network by 
arranging several Network in Network (NiN)  
blocks which have been proposed by Lin \etal~\cite{LIN14}. A NiN block is composed of one $k\times k$ convolution followed by several $1\times1$ convolutions.

\textbf{Feature Extraction.}
\label{sec:method:network_architecture:feature_extraction}
We encourage the network to extract a rich feature representation for 
each modality individually. Therefore, we first process the RGB and 
LiDAR depth map separately, resulting in two parallel data network 
streams. For the RGB part we use the weights and architecture proposed 
by Lin \etal for ImageNet \cite{DENG09} classification. However, we 
skip the last NiN block as we're only interested in the feature extraction part 
and not in image classification. The depth stream is kept symmetrically but with a fewer number of feature channels as this part is learned from scratch.

\textbf{Feature Matching.}
\label{sec:method:network_architecture:feature_matching}
After extracting features from both input modalities the feature maps are concatenated to fuse the information from both modalities. This part of the network is also realized as a stack of NiN blocks. By convolving the stacked LiDAR and RGB features a joint representation is generated. This architecture was motivated by Dosovitskiy \etal~\cite{DOSOVITSKIY16} who also introduced a specific correlation layer. However, they show that their network is capable of correlating features without explicitly demanding it.

\textbf{Global Regression.}
\label{sec:method:network_architecture:global_regression}
To regress the calibration, the global information that has
been extracted from both modalities has to be pooled. This step
is comparable to a global optimization or solver as used in
classical calibration algorithms. To realize a global information fusion we stack two fully connected layers followed by a Euclidean loss function.
Like \cite{KENDALL15} we also experienced that branching the
network to handle translational and rotational components
separately worsened the result.

\subsection{Refinement}
\label{sec:method:refinement}

\textbf{Iterative Refinement.}
The projection of the depth points strongly varies with the given initial calibration as depicted in \Cref{fig:differentMessings}.
Some transformations cause the projection to be mostly outside
the image area, so only few correspondences between the LiDAR and
the RGB image can be established. We noted, that our network is
still able to improve the calibration in those cases. By using the new estimated calibration $\mat{\hat{H}} = \mat{H}_{\text{init}} \decalibguess^{-1}$ we can again project the depth points resulting in more depth points for correlation. This step can then be iterated several times.

\textbf{Temporal Filtering.}
There are only few scenarios where an instant registration between two modalities is required.
In the context of autonomous driving the extrinsic calibration between different sensors might involve more than just one frame.
If the output of the network is analyzed over time by using a moving average, the approach yields more robust results.

\subsection{Training Details}
\label{sec:method:training_details}

The RegNet was developed using the Caffe library introduced
by \cite{JIA14}. After each convolutional layer but the last we add a Rectified-Linear Unit (ReLU). The training of the network is performed with
the Adam solver \cite{KINGMA15}. We use Euclidean loss to infer the deviation from the ground truth decalibration. 
The network is trained for 3 Mio.̉ iterations. We set the parameters of the solver to
the suggested default values $\beta_1~=~0.9$, $\beta_2~=~0.999$
and $\epsilon = 10^{-8}$. The learning rate is fixed
at $\alpha = 10^{-5}$ and the batch size is set to $b = 1$ - an increased
batch size did not improve our results.
For the RGB feature extraction part we initialize the NiN blocks
with ImageNet \cite{DENG09}. The remaining weights are learned
from scratch and are initialized using Xavier initialization \cite{GLOROT10}.