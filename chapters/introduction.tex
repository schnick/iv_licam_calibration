% -*- root: ../root.tex -*-
%
% -*- root: ../root.tex -*-
%
\label{sec:introduction}


To acquire a redundant and powerful system for autonomous driving, recent
developments rely on a variety of optical sensors. 
Especially the fusion of camera and depth sensors has therefore been
studied intensively in the last few years. To combine the information of those
sensors, a common world coordinate system has to be defined in respect to which the
sensors' poses are given. Transforming
a point $\vec{x}$ given in the sensor coordinate system into a point $\vec{y}$
in the world coordinate system is typically modeled via an affine
transformation matrix $\mat{H}$, \ie
\begin{equation}
\vec{y} = \mat{H}\vec{x} \punctspace .
\end{equation}
The task of estimating the transformation matrix $\mat{H}$ is called
\emph{extrinsic calibration} and has been studied for a variety of sensor
modalities and combinations. Most approaches can be divided into three steps:
\begin{enumerate}
\item Find distinct features in the sensor data, \eg corners or artificial
targets;
\item Use those features to establish correspondences between the
sensors;
\item Given the correspondences, determine $\mat{H}$ by solving a system of
equations or by minimizing an error function.
\end{enumerate}

\begin{figure}
	\centering%
	\begin{subfigure}[b]{\linewidth}
		\caption*{Decalibrated System}%
		\includegraphics[width=\textwidth]{figures/png/outfile_000000002606_final_messed.png} %
	\end{subfigure}
	
	\vspace{2mm}
	
	\begin{subfigure}[b]{\linewidth}
		\caption*{Ground Truth}%
		\includegraphics[width=\textwidth]{figures/png/outfile_000000002606_final_gt.png} %
	\end{subfigure}
	
	\vspace{2mm}
	
	\begin{subfigure}[b]{\linewidth}
		\caption*{\emph{RegNet} Calibration}%
		\includegraphics[width=\textwidth]{figures/png/outfile_000000002606_final_ours.png} %
	\end{subfigure}
	\caption{\emph{RegNet} is able to correct even large decalibrations such as depicted in the top image. The inputs for the deep neural network are an RGB image and a projected depth map. \emph{RegNet} is able to establish correspondences between the two modalities which enables it to estimate a \num{6} DOF extrinsic calibration.}
	\label{fig:titlePicture}
\end{figure}

The extraction of distinct features can be challenging as correspondences have
to be made across different sensor modalities. Most offline calibration
approaches therefore rely on special calibration targets which provide strong
and distinct signals in all modalities, allowing for an easy detection and
localization\cite{GEIGER12}\cite{MIRZAEI12}\cite{ZHANG04}. However, those approaches are time consuming as they need human
interaction for feature selection or they have to be performed in a controlled
environment. Therefore, several online calibration methods have been proposed
recently\cite{BILESCHI09}\cite{LEVINSON13}\cite{PANDEY15}\cite{CHIEN16}.
The challenging part in online calibration is to find matching
patterns in an unstructured environment. Most of the state-of-the-art
approaches do so by using handcrafted features such as image edges. Because
the descriptors of such features are often not discriminative, the matching
fails, and the subsequent optimization does not lead to satisfying results;
especially when facing large calibration errors.

In this work, we present \emph{RegNet}, the first CNN to fully regress the extrinsic calibration between sensors of different
modalities. We solve the tasks of feature extraction, feature matching, and
global optimization in real-time by using an end-to-end trained deep neural network. 
We propose a simple yet effective sampling strategy
which allows us to generate an infinite amount of training data from only one
manually conducted extrinsic calibration. After the network has been trained,
our approach does not need any further human interaction. This is a huge
advantage for \eg series production of autonomous vehicles where only a single
car has to be calibrated manually in order to train \emph{RegNet}, which
then calibrates all remaining vehicles. Furthermore, the network is able to monitor
and correct calibration errors online, without the need of returning to a
controlled environment for recalibration.