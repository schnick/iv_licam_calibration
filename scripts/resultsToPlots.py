#!/usr/bin/python

class PlotClass():

    def __init__( self ):
        plt.ion()
        #scaled RGB image
        self.f1, self.visuAxis = plt.subplots( 2, 2 )
        self.visuAxis = self.visuAxis.ravel()
        plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0.1, hspace=0)
        figManager = plt.get_current_fig_manager()
        figManager.resize(*figManager.window.maxsize())

        self.f2, self.plotAxis = plt.subplots( 2, 3 )
        self.plotAxis = self.plotAxis.ravel()
        figManager = plt.get_current_fig_manager()
        figManager.resize(*figManager.window.maxsize())


        plt.show()

    def prepareVisu( self ):

    def plotDeviation( self ):

        plt.figure( self.f2.number )

        deviation = [ np.array( x["messed"] ) - np.array( x["ours"] ) for x in self.results ]
        print deviation[0][0]
        xDev = [ x[0] for x in deviation ]
        yDev = [ x[1] for x in deviation ]
        zDev = [ x[2] for x in deviation ]
        yawDev   = [ x[3] for x in deviation ]
        pitchDev = [ x[4] for x in deviation ]
        rollDev  = [ x[5] for x in deviation ]

        #start to show

        if self.firstShow:
            self.xDev, = self.plotAxis[0].plot( xDev, 'b-' )
            self.plotAxis[0].set_ylim([-0.3,0.3 ])
            self.plotAxis[0].set_title('xDev')
            self.yDev, = self.plotAxis[1].plot( yDev, 'b-' )
            self.plotAxis[1].set_ylim([-0.3,0.3 ])
            self.plotAxis[1].set_title('yDev')
            self.zDev, = self.plotAxis[2].plot( zDev, 'b-' )
            self.plotAxis[2].set_ylim([-0.3,0.3 ])
            self.plotAxis[2].set_title('zDev')
            self.pitchDev, = self.plotAxis[3].plot( pitchDev, 'b-' )
            self.plotAxis[3].set_ylim([-2,2])
            self.plotAxis[3].set_title('pitchDev')
            self.yawDev, = self.plotAxis[4].plot( yawDev, 'b-' )
            self.plotAxis[4].set_ylim([-2,2 ])
            self.plotAxis[4].set_title('yawDev')
            self.rollDev, = self.plotAxis[5].plot( rollDev, 'b-' )
            self.plotAxis[5].set_ylim([-2,2 ])
            self.plotAxis[5].set_title('rollDev')
        else:
            self.xDev.set_ydata( xDev )
            self.yDev.set_ydata( yDev )
            self.zDev.set_ydata( zDev )
            self.yawDev.set_ydata( yawDev )
            self.pitchDev.set_ydata( pitchDev )
            self.rollDev.set_ydata( rollDev )

            self.xDev.set_xdata( range( len(xDev) ) )
            self.plotAxis[0].set_xlim([0, len(xDev)])
            self.yDev.set_xdata( range( len(yDev) ) )
            self.plotAxis[1].set_xlim([0, len(xDev)])
            self.zDev.set_xdata( range( len(zDev) ) )
            self.plotAxis[2].set_xlim([0, len(xDev)])
            self.pitchDev.set_xdata( range( len(pitchDev) ) )
            self.plotAxis[3].set_xlim([0, len(xDev)])
            self.yawDev.set_xdata( range( len(yawDev) ) )
            self.plotAxis[4].set_xlim([0, len(xDev)])
            self.rollDev.set_xdata( range( len(rollDev) ) )
            self.plotAxis[5].set_xlim([0, len(xDev)])


        plt.ioff()
        plt.draw()