# README #

This is the latex code for the paper
RegNet: Multimodal Sensor Registration Using Deep Neural Networks
https://arxiv.org/abs/1707.03167


### What is this repository for? ###

* Provides latex code and images to compile the paper
* 1.0

### How do I get set up? ###

* clone the repo
* open the main file and build it with either texstudio or your favorite tex editor